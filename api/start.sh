#!/bin/bash

if [ "$NODE_ENV" == "development" ]
then
  # Use Nodemon to watch for changes and restart the server when in development
  # mode.
  nodemon -x "node --use_strict" -w src src/server.js
else
  # When not in development mode, just start the server.
  node --use_strict src/server.js
fi
